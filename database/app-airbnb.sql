CREATE DATABASE db_airbnb; 

-- ----------------------------
-- Table structure for NguoiDung
-- ----------------------------
DROP TABLE IF EXISTS `NguoiDung`;
CREATE TABLE NguoiDung (
	id int PRIMARY KEY AUTO_INCREMENT,
	name varchar(255),
	email varchar(255),
	pass_word varchar(255),
	phone varchar(255),
	birth_day varchar(255),
	gender varchar(255),
	role varchar(255),
	avatar varchar(255)
);
-- ----------------------------
-- Records of NguoiDung
-- ----------------------------
INSERT INTO NguoiDung VALUES (0,'Tuan Anh','anhtran@gmail.com','admin','0909572536','05-06-1984','Male','ADMIN','');


-- ----------------------------
-- Table structure for ViTri
-- ----------------------------
DROP TABLE IF EXISTS `ViTri`;
CREATE TABLE ViTri (
	id int PRIMARY KEY AUTO_INCREMENT,
	ten_vi_tri varchar(255),
	tinh_thanh varchar(255),
	quoc_gia varchar(255),
	hinh_anh varchar(255)
);
-- ----------------------------
-- Records of ViTri
-- ----------------------------
INSERT INTO ViTri VALUES (0,'Gia Lam','Lam Dong','Viet Nam','');


-- ----------------------------
-- Table structure for DatPhong
-- ----------------------------
DROP TABLE IF EXISTS `DatPhong`;
CREATE TABLE DatPhong (
	id int PRIMARY KEY AUTO_INCREMENT,
	ma_phong int,
	ngay_den datetime,
	ngay_di datetime,
	so_luong_khach int,
	ma_nguoi_dat int,
	FOREIGN KEY (ma_nguoi_dat) REFERENCES NguoiDung(id),
	FOREIGN KEY (ma_phong) REFERENCES Phong(id)
);
-- ----------------------------
-- Records of DatPhong
-- ----------------------------
INSERT INTO DatPhong VALUES (0,1,'2022-12-20','2022-12-21',4,1);


-- ----------------------------
-- Table structure for BinhLuan
-- ----------------------------
DROP TABLE IF EXISTS `BinhLuan`;
CREATE TABLE BinhLuan (
	id int PRIMARY KEY AUTO_INCREMENT,
	ma_phong int,
	ma_nguoi_binh_luan int,
	ngay_binh_luan datetime,
	noi_dung varchar(255),
	sao_binh_luan int,
	FOREIGN KEY (ma_nguoi_binh_luan) REFERENCES NguoiDung(id),
	FOREIGN KEY (ma_phong) REFERENCES Phong(id)	
);
-- ----------------------------
-- Records of BinhLuan
-- ----------------------------
INSERT INTO BinhLuan VALUES (0,1,1,'2022-12-22 22:45:05','Cho o tuyet voi cho gia dinh',5);


-- ----------------------------
-- Table structure for Phong
-- ----------------------------
DROP TABLE IF EXISTS `Phong`;
CREATE TABLE Phong (
	id int PRIMARY KEY AUTO_INCREMENT,
	ten_phong varchar(255),
	khach int,
	phong_ngu int,
	giuong int,
	phong_tam int,
	mo_ta varchar(255),
	gia_tien int,
	may_giat boolean,
	ban_la boolean,
	tivi boolean,
	dieu_hoa boolean,
	wifi boolean,
	bep boolean,
	do_xe boolean,
	ho_boi boolean,
	ban_ui boolean,
	hinh_anh varchar(255),
	vi_tri_id int,
	FOREIGN KEY (vi_tri_id) REFERENCES ViTri(id)
);

-- ----------------------------
-- Records of Phong
-- ----------------------------
INSERT INTO Phong VALUES (0,'Gác Mây',20,8,8,8,'Homestay nằm trên đồi, tạo cảm giác yên bình, hòa mình vào thiên nhiên. Là nơi để du khách quên đi mệt mỏi, bộn bề cuộc sống, hít thở không khí trong lành',40,true,false,false,false,true,true,true,false,false,'NULL',1);
