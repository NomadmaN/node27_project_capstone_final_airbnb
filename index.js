const express = require("express");
const app = express();
app.use(express.json());
app.use(express.static("."));
const cors = require("cors");
const corsOptions = {
    origin: 'https://backend-airbnb.netlify.app',
    optionsSuccessStatus: 200
  };
app.use(cors(corsOptions));
app.listen(8080);
// api
const rootRoute = require("./src/routes/rootRoute");
app.use("/api", rootRoute);
// swagger
const swaggerUi = require('swagger-ui-express');
const swaggerSpec = require('./swagger/swagger.json');
app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.use('/swagger', express.static('swagger'));
// upload
app.use('/public/img', express.static('./public/img'))