const response = (payload, ...rest) => {
    return {
      status: "succesfully",
      data: payload,
      ...rest,
    };
  };
  
  module.exports = {
    response,
  };
  