const express = require("express");
const bookingRoute = express.Router();
const { verifyToken } = require("../utilities/jwtoken");

const {
  getBookingAll,
  postNewBooking,
  getBookingById,
  updateBooking,
  deleteBooking,
  getBookingByUserId,
} = require("../controllers/bookingController");

bookingRoute.get("", verifyToken, getBookingAll);
bookingRoute.post("", verifyToken, postNewBooking);
bookingRoute.get("/:id", verifyToken, getBookingById);
bookingRoute.put("/:id", verifyToken, updateBooking);
bookingRoute.delete("/:id", verifyToken, deleteBooking);
bookingRoute.get("/lay-theo-nguoi-dung/:id", verifyToken, getBookingByUserId);

module.exports = bookingRoute;
