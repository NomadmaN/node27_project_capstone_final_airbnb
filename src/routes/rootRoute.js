const express = require('express');
const rootRoute = express.Router();
const authRoute = require('./authRoute');
const commentRoute = require('./commentRoute');
const bookingRoute = require('./bookingRoute');
const userRoute = require('./userRoute');
const roomRoute = require('./roomRoute');
const locationRoute = require('./locationRoute');

rootRoute.use("/auth", authRoute);
rootRoute.use("/binh-luan", commentRoute);
rootRoute.use("/dat-phong", bookingRoute);
rootRoute.use("/users", userRoute);
rootRoute.use("/phong-thue", roomRoute);
rootRoute.use("/vi-tri", locationRoute);

module.exports = rootRoute;