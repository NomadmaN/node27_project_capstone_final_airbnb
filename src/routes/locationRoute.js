const express = require("express");
const locationRoute = express.Router();
const { verifyToken } = require("../utilities/jwtoken");

const {
  getLocationAll,
  postNewLocation,
  getLocationPagination,
  getLocationById,
  updateLocation,
  deleteLocation,
  postUploadLocationImg,
} = require("../controllers/locationController");

locationRoute.get("", verifyToken, getLocationAll);
locationRoute.post("", verifyToken, postNewLocation);
locationRoute.get("/phan-trang-tim-kiem", verifyToken, getLocationPagination);
locationRoute.get("/:id", verifyToken, getLocationById);
locationRoute.put("/:id", verifyToken, updateLocation);
locationRoute.delete("/:id", verifyToken, deleteLocation);
locationRoute.post("/upload-hinh-vitri", verifyToken, postUploadLocationImg);

module.exports = locationRoute;
