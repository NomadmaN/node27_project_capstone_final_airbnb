const express = require("express");
const roomRoute = express.Router();
const { verifyToken } = require("../utilities/jwtoken");

const {
  getRoomAll,
  postNewRoom,
  getRoomByLocationId,
  getRoomPagination,
  getRoomById,
  updateRoom,
  deleteRoom,
  postUploadRoomImg,
} = require("../controllers/roomController");

roomRoute.get("", verifyToken, getRoomAll);
roomRoute.post("", verifyToken, postNewRoom);
roomRoute.get("/lay-phong-theo-vi-tri/:id", verifyToken, getRoomByLocationId);
roomRoute.get("/phan-trang-tim-kiem", verifyToken, getRoomPagination);
roomRoute.get("/:id", verifyToken, getRoomById);
roomRoute.put("/:id", verifyToken, updateRoom);
roomRoute.delete("/:id", verifyToken, deleteRoom);
roomRoute.post("/upload-hinh-phong", verifyToken, postUploadRoomImg);

module.exports = roomRoute;
