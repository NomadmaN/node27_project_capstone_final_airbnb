const express = require("express");
const userRoute = express.Router();
const { verifyToken } = require("../utilities/jwtoken");

const {
  getUserAll,
  postNewUser,
  deleteUser,
  getUserPagination,
  getUserById,
  updateUser,
  getUserSearched,
  postUploadAvatar,
} = require("../controllers/userController");

userRoute.get("", verifyToken, getUserAll);
userRoute.post("", verifyToken, postNewUser);
userRoute.delete("/:id", verifyToken, deleteUser);
userRoute.get("/phan-trang-tim-kiem", verifyToken, getUserPagination);
userRoute.get("/:id", verifyToken, getUserById);
userRoute.put("/:id", verifyToken, updateUser);
userRoute.get("/search/:name", verifyToken, getUserSearched);
userRoute.post("/upload-avatar", verifyToken, postUploadAvatar);

module.exports = userRoute;
