const express = require("express");
const commentRoute = express.Router();
const { verifyToken } = require("../utilities/jwtoken");

const {
  getCommentAll,
  postNewComment,
  updateComment,
  deleteComment,
  getCommentByRoomId,
} = require("../controllers/commentController");

commentRoute.get("", verifyToken, getCommentAll);
commentRoute.post("", verifyToken, postNewComment);
commentRoute.put("/:id", verifyToken, updateComment);
commentRoute.delete("/:id", verifyToken, deleteComment);
commentRoute.get("/lay-binh-luan-theo-phong/:id", verifyToken, getCommentByRoomId);

module.exports = commentRoute;
