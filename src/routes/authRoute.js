const express = require("express");
const authRoute = express.Router();

const {
  signup,
  signin,
} = require("../controllers/authController");

authRoute.post("/signup", signup);
authRoute.post("/signin", signin);

module.exports = authRoute;
