// Import sequelize
const sequelize = require("../models/index");
const initModels = require("../models/init-models");
const model = initModels(sequelize);
const { successCode, failCode, failCode404 } = require("../config/response");
const bcrypt = require("bcrypt");
const multer = require("multer");
const fs = require("fs");
const { Op } = require("sequelize");

// get rooms
const getRoomAll = async (req, res) => {
  try {
    let data = await model.phong.findAll();
    successCode(res, data, "Lấy dữ liệu thành công");
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// post new Room
const postNewRoom = async (req, res) => {
  try {
    let { tenPhong, khach, phongNgu, giuong, phongTam, moTa, giaTien, mayGiat, banLa, tivi, dieuHoa, wifi, bep, doXe, hoBoi, banUi, maViTri, hinhAnh } = req.body;
    let newRoom = {
      ten_phong: tenPhong,
      khach: khach,
      phong_ngu: phongNgu,
      giuong: giuong,
      phong_tam: phongTam,
      mo_ta: moTa,
      gia_tien: giaTien,
      may_giat: mayGiat,
      ban_la: banLa,
      tivi: tivi,
      dieu_hoa: dieuHoa,
      wifi: wifi,
      bep: bep,
      do_xe: doXe,
      ho_boi: hoBoi,
      ban_ui: banUi,
      hinh_anh: hinhAnh,
      vi_tri_id: maViTri
    };
    let dataRoom = await model.phong.create(newRoom);
    console.log("dataRoom == ", dataRoom);
    if (dataRoom) {
      // res.status(200).send("Đã thêm phòng thành công!");
      successCode(res, dataRoom, "Đã thêm phòng thành công!");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// get Room by Location id
const getRoomByLocationId = async (req, res) => {
  try {
    let { id } = req.params;
    let dataFiltered = await model.phong.findAll({
      where: {
        vi_tri_id: id,
      },
      include:["vi_tri"],
    });
    if (dataFiltered) {
      res.status(200).send(dataFiltered);
    } else {
      res.status(400).send("Vị trí không tồn tại");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// get Rooms by pagination
const getRoomPagination = async (req, res) => {
  try {
    // extract pageIndex, pageSize, and keyword from the request object
    const pageIndex = parseInt(req.query.pageIndex) || 0;
    const pageSize = parseInt(req.query.pageSize) || 10;
    const keyword = req.query.keyword || "";

    const data = await model.phong.findAndCountAll({
      where: {
        [Op.or]: [
          { ten_phong: { [Op.like]: `%${keyword}%` } },
          // add more columns to search as needed
        ],
      },
      limit: pageSize,
      offset: pageIndex * pageSize,
    });

    successCode(res, data, "Lấy dữ liệu thành công");
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// get Room by Room id
const getRoomById = async (req, res) => {
  try {
    let { id } = req.params;
    let dataFiltered = await model.phong.findOne({
      where: {
        id: id,
      },
    });
    if (dataFiltered) {
      res.status(200).send(dataFiltered);
    } else {
      res.status(400).send("Phòng không tồn tại");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// put edit & update Room
const updateRoom = async (req, res) => {
  try {
    let { id } = req.params;
    console.log("id ==", id);

    let dataFiltered = await model.phong.findOne({
      where: {
        id: id,
      },
    });
    if (dataFiltered) {
      let { tenPhong, khach, phongNgu, giuong, phongTam, moTa, giaTien, mayGiat, banLa, tivi, dieuHoa, wifi, bep, doXe, hoBoi, banUi, maViTri, hinhAnh } = req.body;
      let roomUpdate = {
        ten_phong: tenPhong,
        khach: khach,
        phong_ngu: phongNgu,
        giuong: giuong,
        phong_tam: phongTam,
        mo_ta: moTa,
        gia_tien: giaTien,
        may_giat: mayGiat,
        ban_la: banLa,
        tivi: tivi,
        dieu_hoa: dieuHoa,
        wifi: wifi,
        bep: bep,
        do_xe: doXe,
        ho_boi: hoBoi,
        ban_ui: banUi,
        hinh_anh: hinhAnh,
        vi_tri_id: maViTri
      };

      let dataUpdate = await model.phong.update(roomUpdate, {
        where: {
          id: id,
        },
      });

      if (dataUpdate[0] == 1) {
        // res.status(200).send("Cập nhật phòng thành công");
        successCode(res, roomUpdate, "Cập nhật phòng thành công!");
      } else {
        res.status(400).send("Không có gì mới để cập nhật");
      }
    } else {
      res.status(400).send("Phòng không tồn tại");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// delete Room
const deleteRoom = async (req, res) => {
  try {
    let { id } = req.params;
    let dataFiltered = await model.phong.destroy({
      where: {
        id: id,
      },
    });
    if (dataFiltered) {
      res.status(200).send("Xóa phòng thành công");
    } else {
      res.status(400).send("Phòng không tồn tại");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// Room upload avatar
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./public/img"); // set the destination folder for uploaded files
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "_" + file.originalname); // set the filename for uploaded files
  },
});
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 1, // set the maximum file size to 1MB
  },
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype === "image/jpeg" ||
      file.mimetype === "image/png" ||
      file.mimetype === "image/gif"
    ) {
      cb(null, true); // accept files with the specified mimetypes
    } else {
      cb(new Error("Invalid file type."), false); // reject files with other mimetypes
    }
  },
});
const postUploadRoomImg = (req, res) => {
  try {
    upload.single("file")(req, res, function (err) {
      if (err) {
        console.log("Error == ", err);
        // Handle any errors that occurred during the upload
        return res.status(400).send("No file uploaded.");
      } else {
        // Return a success message if the upload was successful
        return res.status(200).json({
          message: "Hình phòng được upload thành công!",
          filename: req.file.filename,
          mimetype: req.file.mimetype,
          size: req.file.size,
          stored: req.file.destination,
        });
      }
    });
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

module.exports = {
  getRoomAll,
  postNewRoom,
  getRoomByLocationId,
  getRoomPagination,
  getRoomById,
  deleteRoom,
  updateRoom,
  postUploadRoomImg,
};
