// Import sequelize
const sequelize = require("../models/index");
const initModels = require("../models/init-models");
const model = initModels(sequelize);
const { successCode, failCode, failCode404 } = require("../config/response");
const bcrypt = require("bcrypt");
const multer = require("multer");
const fs = require("fs");
const { Op } = require("sequelize");

// get Locations
const getLocationAll = async (req, res) => {
  try {
    let data = await model.vitri.findAll();
    successCode(res, data, "Lấy dữ liệu thành công");
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// post new Location
const postNewLocation = async (req, res) => {
  try {
    let { tenViTri, tinhThanh, quocGia, hinhAnh } = req.body;
    let newLocation = {
      ten_vi_tri: tenViTri,
      tinh_thanh: tinhThanh,
      quoc_gia: quocGia,
      hinh_anh: hinhAnh,
    };
    let dataLocation = await model.vitri.create(newLocation);
    console.log("dataLocation == ", dataLocation);
    if (dataLocation) {
      // res.status(200).send("Đã thêm vị trí thành công!");
      successCode(res, dataLocation, "Đã thêm vị trí thành công!");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// get Locations by pagination
const getLocationPagination = async (req, res) => {
  try {
    // extract pageIndex, pageSize, and keyword from the request object
    const pageIndex = parseInt(req.query.pageIndex) || 0;
    const pageSize = parseInt(req.query.pageSize) || 10;
    const keyword = req.query.keyword || "";

    const data = await model.vitri.findAndCountAll({
      where: {
        [Op.or]: [
          { ten_vi_tri: { [Op.like]: `%${keyword}%` } },
          // add more columns to search as needed
        ],
      },
      limit: pageSize,
      offset: pageIndex * pageSize,
    });

    successCode(res, data, "Lấy dữ liệu thành công");
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// get Location by Location id
const getLocationById = async (req, res) => {
  try {
    let { id } = req.params;
    let dataFiltered = await model.vitri.findOne({
      where: {
        id: id,
      },
    });
    if (dataFiltered) {
      res.status(200).send(dataFiltered);
    } else {
      res.status(400).send("Vị trí không tồn tại");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// put edit & update Location
const updateLocation = async (req, res) => {
  try {
    let { id } = req.params;
    console.log("id ==", id);

    let dataFiltered = await model.vitri.findOne({
      where: {
        id: id,
      },
    });
    if (dataFiltered) {
      let { tenViTri, tinhThanh, quocGia, hinhAnh } = req.body;
      let locationUpdate = {
        ten_vi_tri: tenViTri,
        tinh_thanh: tinhThanh,
        quoc_gia: quocGia,
        hinh_anh: hinhAnh,
      };

      let dataUpdate = await model.vitri.update(locationUpdate, {
        where: {
          id: id,
        },
      });

      if (dataUpdate[0] == 1) {
        // res.status(200).send("Cập nhật vị trí thành công");
        successCode(res, locationUpdate, "Cập nhật vị trí thành công!");
      } else {
        res.status(400).send("Không có gì mới để cập nhật");
      }
    } else {
      res.status(400).send("Vị trí không tồn tại");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// delete Location
const deleteLocation = async (req, res) => {
  try {
    let { id } = req.params;
    let dataFiltered = await model.vitri.destroy({
      where: {
        id: id,
      },
    });
    if (dataFiltered) {
      res.status(200).send("Xóa vị trí thành công");
    } else {
      res.status(400).send("Vị trí không tồn tại");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// Location upload avatar
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./public/img"); // set the destination folder for uploaded files
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "_" + file.originalname); // set the filename for uploaded files
  },
});
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 1, // set the maximum file size to 1MB
  },
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype === "image/jpeg" ||
      file.mimetype === "image/png" ||
      file.mimetype === "image/gif"
    ) {
      cb(null, true); // accept files with the specified mimetypes
    } else {
      cb(new Error("Invalid file type."), false); // reject files with other mimetypes
    }
  },
});
const postUploadLocationImg = (req, res) => {
  try {
    upload.single("file")(req, res, function (err) {
      if (err) {
        console.log("Error == ", err);
        // Handle any errors that occurred during the upload
        return res.status(400).send("No file uploaded.");
      } else {
        // Return a success message if the upload was successful
        return res.status(200).json({
          message: "Hình vị trí được upload thành công!",
          filename: req.file.filename,
          mimetype: req.file.mimetype,
          size: req.file.size,
          stored: req.file.destination,
        });
      }
    });
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

module.exports = {
  getLocationAll,
  postNewLocation,
  getLocationPagination,
  getLocationById,
  deleteLocation,
  updateLocation,
  postUploadLocationImg,
};
