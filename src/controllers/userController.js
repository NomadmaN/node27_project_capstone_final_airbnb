// Import sequelize
const sequelize = require("../models/index");
const initModels = require("../models/init-models");
const model = initModels(sequelize);
const { successCode, failCode, failCode404 } = require("../config/response");
const bcrypt = require("bcrypt");
const multer = require("multer");
const fs = require("fs");
const { Op } = require("sequelize");

// get users
const getUserAll = async (req, res) => {
  try {
    let data = await model.nguoidung.findAll();
    successCode(res, data, "Lấy dữ liệu thành công");
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// post new user
const postNewUser = async (req, res) => {
  try {
    let { name, email, password, phone, birthday, gender, role } = req.body;
    let newUser = {
      name: name,
      email: email,
      pass_word: bcrypt.hashSync(password, 10),
      phone: phone,
      birth_day: birthday,
      gender: gender,
      role: role,
    };
    let dataUser = await model.nguoidung.create(newUser);
    console.log("dataUser == ", dataUser);
    if (dataUser) {
      // res.status(200).send("Đã thêm người dùng thành công!");
      successCode(res, dataUser, "Đã thêm người dùng thành công!");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// delete user
const deleteUser = async (req, res) => {
  try {
    let { id } = req.params;
    let dataFiltered = await model.nguoidung.destroy({
      where: {
        id: id,
      },
    });
    if (dataFiltered) {
      res.status(200).send("Xóa người dùng thành công");
    } else {
      res.status(400).send("Người dùng không tồn tại");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// get users by pagination
const getUserPagination = async (req, res) => {
  try {
    // extract pageIndex, pageSize, and keyword from the request object
    const pageIndex = parseInt(req.query.pageIndex) || 0;
    const pageSize = parseInt(req.query.pageSize) || 10;
    const keyword = req.query.keyword || "";

    const data = await model.nguoidung.findAndCountAll({
      where: {
        [Op.or]: [
          { name: { [Op.like]: `%${keyword}%` } },
          // add more columns to search as needed
        ],
      },
      limit: pageSize,
      offset: pageIndex * pageSize,
    });

    successCode(res, data, "Lấy dữ liệu thành công");
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// get user by user id
const getUserById = async (req, res) => {
  try {
    let { id } = req.params;
    let dataFiltered = await model.nguoidung.findOne({
      where: {
        id: id,
      },
    });
    if (dataFiltered) {
      res.status(200).send(dataFiltered);
    } else {
      res.status(400).send("Người dùng không tồn tại");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// put edit & update user
const updateUser = async (req, res) => {
  try {
    let { id } = req.params;
    console.log("id ==", id);

    let dataFiltered = await model.nguoidung.findOne({
      where: {
        id: id,
      },
    });
    if (dataFiltered) {
      let { name, email, password, phone, birthday, gender, role } = req.body;
      let userUpdate = {
        name: name,
        email: email,
        pass_word: bcrypt.hashSync(password, 10),
        phone: phone,
        birth_day: birthday,
        gender: gender,
        role: role,
      };

      let dataUpdate = await model.nguoidung.update(userUpdate, {
        where: {
          id: id,
        },
      });

      if (dataUpdate[0] == 1) {
        // res.status(200).send("Cập nhật người dùng thành công");
        successCode(res, userUpdate, "Cập nhật người dùng thành công!");
      } else {
        res.status(400).send("Không có gì mới để cập nhật");
      }
    } else {
      res.status(400).send("Người dùng không tồn tại");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// get users by name searched
const getUserSearched = async (req, res) => {
  try {
    let { name } = req.params;
    let dataFiltered = await model.nguoidung.findAll({
      where: {
        name: {
          [Op.like]: `%${name}%`,
        },
      },
    });
    if (dataFiltered) {
      res.status(200).send(dataFiltered);
    } else {
      res.status(400).send("Người dùng không tồn tại");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// user upload avatar
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./public/img"); // set the destination folder for uploaded files
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "_" + file.originalname); // set the filename for uploaded files
  },
});
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 1, // set the maximum file size to 1MB
  },
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype === "image/jpeg" ||
      file.mimetype === "image/png" ||
      file.mimetype === "image/gif"
    ) {
      cb(null, true); // accept files with the specified mimetypes
    } else {
      cb(new Error("Invalid file type."), false); // reject files with other mimetypes
    }
  },
});
const postUploadAvatar = (req, res) => {
  try {
    upload.single("file")(req, res, function (err) {
      if (err) {
        console.log("Error == ", err);
        // Handle any errors that occurred during the upload
        return res.status(400).send("No file uploaded.");
      } else {
        // Return a success message if the upload was successful
        return res.status(200).json({
          message: "Hình avatar được upload thành công!",
          filename: req.file.filename,
          mimetype: req.file.mimetype,
          size: req.file.size,
          stored: req.file.destination,
        });
      }
    });
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

module.exports = {
  getUserAll,
  postNewUser,
  deleteUser,
  getUserPagination,
  getUserById,
  updateUser,
  getUserSearched,
  postUploadAvatar,
};
