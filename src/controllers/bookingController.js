// Import sequelize
const sequelize = require("../models/index");
const initModels = require("../models/init-models");
const model = initModels(sequelize);
const { successCode, failCode } = require("../config/response");
const { Op } = require("sequelize");

// get bookings
const getBookingAll = async (req, res) => {
  try {
    let data = await model.datphong.findAll();
    successCode(res, data, "Lấy dữ liệu thành công");
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// post new booking from user
const postNewBooking = async (req, res) => {
  try {
    let { maPhong, ngayDen, ngayDi, soLuongKhach, maNguoiDung } = req.body;
    let newBooking = {
      ma_phong: maPhong,
      ngay_den: ngayDen,
      ngay_di: ngayDi,
      so_luong_khach: soLuongKhach,
      ma_nguoi_dat: maNguoiDung,
    };
    let dataBooking = await model.datphong.create(newBooking);
    console.log("dataBooking == ", dataBooking);
    if (dataBooking) {
      // res.status(200).send("Đã thêm đặt phòng thành công!");
      successCode(res, dataBooking, "Đã thêm đặt phòng thành công!");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// get booking by booking id
const getBookingById = async (req, res) => {
  try {
    let { id } = req.params;
    let dataFiltered = await model.datphong.findOne({
      where: {
        id: id,
      },
    });
    if (dataFiltered) {
      // res.status(200).send(dataFiltered);
      successCode(res, dataFiltered, "Lấy dữ liệu thành công");
    } else {
      res.status(400).send("Đặt phòng không tồn tại");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// put edit & update booking
const updateBooking = async (req, res) => {
  try {
    let { id } = req.params;
    console.log("id ==", id);

    let dataFiltered = await model.datphong.findOne({
      where: {
        id: id,
      },
    });
    if (dataFiltered) {
      let { maPhong, ngayDen, ngayDi, soLuongKhach, maNguoiDung } = req.body;
      let bookingUpdate = {
        ma_phong: maPhong,
        ngay_den: ngayDen,
        ngay_di: ngayDi,
        so_luong_khach: soLuongKhach,
        ma_nguoi_dat: maNguoiDung,
      };

      let dataUpdate = await model.datphong.update(bookingUpdate, {
        where: {
          id: id,
        },
      });

      if (dataUpdate[0] == 1) {
        // res.status(200).send("Cập nhật đặt phòng thành công");
        successCode(res, bookingUpdate, "Cập nhật đặt phòng thành công");
      } else {
        res.status(400).send("Không có gì mới để cập nhật");
      }
    } else {
      res.status(400).send("Đặt phòng không tồn tại");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// delete booking
const deleteBooking = async (req, res) => {
  try {
    let { id } = req.params;
    let dataFiltered = await model.datphong.destroy({
      where: {
        id: id,
      },
    });
    if (dataFiltered) {
      res.status(200).send("Xóa đặt phòng thành công");
    } else {
      res.status(400).send("Đặt phòng không tồn tại");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// get booking by user id
const getBookingByUserId = async (req, res) => {
  try {
    let { id } = req.params;
    let dataFiltered = await model.datphong.findAll({
      where: {
        ma_nguoi_dat: id,
      },
      // include:["ma_nguoi_binh_luan_nguoidung"],
    });
    if (dataFiltered) successCode(res, dataFiltered, "Lấy dữ liệu thành công");
    else failCode(res, id, "Đặt phòng không tồn tại");
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

module.exports = {
  getBookingAll,
  postNewBooking,
  getBookingById,
  updateBooking,
  deleteBooking,
  getBookingByUserId,
};
