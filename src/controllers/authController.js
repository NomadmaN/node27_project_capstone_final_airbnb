const sequelize = require("../models/index");
const initModels = require("../models/init-models");
const model = initModels(sequelize);
const { successCode, failCode, failCode404 } = require("../config/response");
const { generateToken } = require("../utilities/jwtoken.js");
const bcrypt = require("bcrypt");
const multer = require("multer");
const fs = require("fs");

// sign up
const signup = async (req, res) => {
  try {
    let { name, email, password, phone, birthday, gender, role } = req.body;
    let newUser = {
      name: name,
      email: email,
      pass_word: bcrypt.hashSync(password, 10),
      phone: phone,
      birth_day: birthday,
      gender: gender,
      role: role,
    };

    let data = await model.nguoidung.create(newUser);
    console.log("data signup == ", data);
    if (data) res.status(200).send("Đăng ký user thành công");
  } catch (err) {
    res.status(500).send("Lỗi Back end");
  }
};

// sign in
const signin = async (req, res) => {
  try {
    let { email, password } = req.body;
    let checkUser = await model.nguoidung.findOne({
      where: {
        email: email,
      },
    });
    console.log("check user signin == ", checkUser);
    if (checkUser) {
      let checkPassword = bcrypt.compareSync(password, checkUser.pass_word);
      console.log("password true/false == ", checkPassword);

      if (checkPassword) {
        let accessToken = generateToken(checkUser);
        const response = {
          "accessToken": accessToken,
        }
        successCode(res, response, "Login thành công");
      } else {
        failCode(res, { email, password }, "Mật khẩu không đúng !");
      }
    } else {
      failCode404(res, { email, password }, "Email không tồn tại !");
    }
  } catch (err) {
    console.log(err);
    res.status(500).send("Lỗi Back end");
  }
};

module.exports = {
  signup,
  signin,
};
