// Import sequelize
const sequelize = require("../models/index");
const initModels = require("../models/init-models");
const model = initModels(sequelize);
const { successCode, failCode } = require("../config/response");
const { Op } = require("sequelize");

// get comments
const getCommentAll = async (req, res) => {
  try {
    let data = await model.binhluan.findAll();
    successCode(res, data, "Lấy dữ liệu thành công");
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// post new comment from user
const postNewComment = async (req, res) => {
  try {
    let { maPhong, maNguoiBinhLuan, ngayBinhLuan, noiDung, saoBinhLuan } =
      req.body;
    let newComment = {
      ma_phong: maPhong,
      ma_nguoi_binh_luan: maNguoiBinhLuan,
      ngay_binh_luan: ngayBinhLuan,
      noi_dung: noiDung,
      sao_binh_luan: saoBinhLuan,
    };
    let dataComment = await model.binhluan.create(newComment);
    console.log("dataComment == ", dataComment);
    if (dataComment) {
      // res.status(200).send("Đã thêm bình luận thành công!");
      successCode(res, dataComment, "Đã thêm bình luận thành công!");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// put edit & update comment
const updateComment = async (req, res) => {
  try {
    let { id } = req.params;
    console.log("id ==", id);

    let dataFiltered = await model.binhluan.findOne({
      where: {
        id: id,
      },
    });
    if (dataFiltered) {
      let { maPhong, maNguoiBinhLuan, ngayBinhLuan, noiDung, saoBinhLuan } =
        req.body;
      let commentUpdate = {
        ma_phong: maPhong,
        ma_nguoi_binh_luan: maNguoiBinhLuan,
        ngay_binh_luan: ngayBinhLuan,
        noi_dung: noiDung,
        sao_binh_luan: saoBinhLuan,
      };

      let dataUpdate = await model.binhluan.update(commentUpdate, {
        where: {
          id: id,
        },
      });

      if (dataUpdate[0] == 1) {
        // res.status(200).send("Cập nhật bình luận thành công");
        successCode(res, commentUpdate, "Cập nhật bình luận thành công!");
      } else {
        res.status(400).send("Không có gì mới để cập nhật");
      }
    } else {
      res.status(400).send("Bình luận không tồn tại");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// delete comment
const deleteComment = async (req, res) => {
  try {
    let { id } = req.params;
    let dataFiltered = await model.binhluan.destroy({
      where: {
        id: id,
      },
    });
    if (dataFiltered) {
      res.status(200).send("Xóa bình luận thành công");
    } else {
      res.status(400).send("Bình luận không tồn tại");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

// get comment by room id
const getCommentByRoomId = async (req, res) => {
  try {
    let { id } = req.params;
    let dataFiltered = await model.binhluan.findAll({
      where: {
        ma_phong: id
      },
      // include:["ma_nguoi_binh_luan_nguoidung"],
    });
    if (dataFiltered)
            successCode(res, dataFiltered, "Lấy dữ liệu thành công")
        else
            failCode(res, id, "Phòng không tồn tại");
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

module.exports = {
  getCommentAll,
  postNewComment,
  updateComment,
  deleteComment,
  getCommentByRoomId
};
