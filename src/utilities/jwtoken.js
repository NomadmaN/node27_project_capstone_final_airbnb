const jwt = require("jsonwebtoken");

const generateToken = (data) => {
  let token = jwt.sign({ content: data }, process.env.SECRET_KEY, {
    expiresIn: "6h",
  });
  return token;
};

const checkToken = (accessToken) => {
  try {
    // Verify the access token using the secret key
    const decoded = jwt.verify(accessToken, process.env.SECRET_KEY);
    return decoded;
  } catch (err) {
    // If the access token is invalid, throw an error
    throw new Error("Invalid access token");
  }
};

const verifyToken = (req, res, next) => {
  try {
    const accessToken =
      req.body.accessToken ||
      req.query.accessToken ||
      req.headers.authorization;
    if (!accessToken) {
      return res.status(401).send("Thiếu accessToken");
    }
    let check = checkToken(accessToken);
    if (check) {
      req.user = check.content; // save user information in the request object for future use
      return next(); //nhảy qua tham số tiếp theo
    } else {
      return res.status(401).send("accessToken không đúng hoặc hết hạn");
    }
  } catch (err) {
    return res.status(500).send(err.message);
  }
};

module.exports = {
  checkToken,
  generateToken,
  verifyToken,
};
