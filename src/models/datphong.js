const Sequelize = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  return datphong.init(sequelize, DataTypes);
}

class datphong extends Sequelize.Model {
  static init(sequelize, DataTypes) {
  return super.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    ma_phong: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'phong',
        key: 'id'
      }
    },
    ngay_den: {
      type: DataTypes.DATE,
      allowNull: true
    },
    ngay_di: {
      type: DataTypes.DATE,
      allowNull: true
    },
    so_luong_khach: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    ma_nguoi_dat: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'nguoidung',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'datphong',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "ma_nguoi_dat",
        using: "BTREE",
        fields: [
          { name: "ma_nguoi_dat" },
        ]
      },
      {
        name: "ma_phong",
        using: "BTREE",
        fields: [
          { name: "ma_phong" },
        ]
      },
    ]
  });
  }
}
