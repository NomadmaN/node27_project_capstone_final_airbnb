const DataTypes = require("sequelize").DataTypes;
const _binhluan = require("./binhluan");
const _datphong = require("./datphong");
const _nguoidung = require("./nguoidung");
const _phong = require("./phong");
const _vitri = require("./vitri");

function initModels(sequelize) {
  const binhluan = _binhluan(sequelize, DataTypes);
  const datphong = _datphong(sequelize, DataTypes);
  const nguoidung = _nguoidung(sequelize, DataTypes);
  const phong = _phong(sequelize, DataTypes);
  const vitri = _vitri(sequelize, DataTypes);

  binhluan.belongsTo(nguoidung, { as: "ma_nguoi_binh_luan_nguoidung", foreignKey: "ma_nguoi_binh_luan"});
  nguoidung.hasMany(binhluan, { as: "binhluans", foreignKey: "ma_nguoi_binh_luan"});
  datphong.belongsTo(nguoidung, { as: "ma_nguoi_dat_nguoidung", foreignKey: "ma_nguoi_dat"});
  nguoidung.hasMany(datphong, { as: "datphongs", foreignKey: "ma_nguoi_dat"});
  binhluan.belongsTo(phong, { as: "ma_phong_phong", foreignKey: "ma_phong"});
  phong.hasMany(binhluan, { as: "binhluans", foreignKey: "ma_phong"});
  datphong.belongsTo(phong, { as: "ma_phong_phong", foreignKey: "ma_phong"});
  phong.hasMany(datphong, { as: "datphongs", foreignKey: "ma_phong"});
  phong.belongsTo(vitri, { as: "vi_tri", foreignKey: "vi_tri_id"});
  vitri.hasMany(phong, { as: "phongs", foreignKey: "vi_tri_id"});

  return {
    binhluan,
    datphong,
    nguoidung,
    phong,
    vitri,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
